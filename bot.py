import requests
import sqlite3
import pandas as pd
import time
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup


class Crawler:
    def __init__(self):
        self.con = sqlite3.connect("hardwareswap.db")
        with open("itemsofinterest.txt", "r") as file:
            self.itemlist = [x.strip("\n").lower() for x in file.readlines()]

    def get_posts(self, number=10):
        """
            Retrieve postings from reddit that have price offers for specified items.
        """
        url = "https://api.pushshift.io/reddit/search/submission"
        params = {"subreddit": "hardwareswap", "size": number}

        r = requests.get(url, params=params)

        j = r.json()

        return j

    def log(self, datadf):
        """
            Write new information to a database of part prices.
        """
        con = self.con
        for item in self.itemlist:
            try:
                prev_data = pd.read_sql(f'SELECT * FROM "{item}"', con)
            except:
                ph = pd.DataFrame.from_dict(
                    {"item": [item], "title": ["a"], "date_created": [0], "price": [0]}
                )
                ph.to_sql(item, con, if_exists="replace")
                prev_data = pd.read_sql(f'SELECT * FROM "{item}"', con)
            try:
                tempdf = datadf.groupby("item").get_group(item)
                prev_data = prev_data.drop("index", axis=1)
                concatdf = tempdf.merge(prev_data, how="outer", indicator=True).loc[
                    lambda x: x["_merge"] == "left_only"
                ]
                concatdf = concatdf.drop("_merge", axis=1)
                concatdf.to_sql(item, con, if_exists="append")

            except:
                print(f"{item} not in groups.")

    def parse_json(self, json):
        """
            prepare dataframe of information for the targeted item
        """
        datadf = pd.DataFrame.from_dict(
            {"item": [], "title": [], "date_created": [], "price": []}
        )
        for item in json["data"]:
            for word in item["title"].split():
                if word.lower() in self.itemlist:
                    price = self.extract_price(item["selftext"], word.lower())
                    datadf = datadf.append(
                        pd.DataFrame.from_dict(
                            {
                                "item": [word.lower()],
                                "title": [item["title"]],
                                "date_created": [item["created_utc"]],
                                "price": price,
                            }
                        ),
                        ignore_index=True,
                    )
        return datadf.dropna().astype({"price": "int64"})

    def get_history(self, item):
        """
            Retrieve the price history for the specifed item.

            item (string): an item with price history in the database. 
        """
        con = self.con
        df = pd.read_sql(f'SELECT * from "{item}"', con)
        return df

    def extract_price(self, text_body, item):
        """
            Extract the price of the specified item from the post body of a reddit post.
        """
        lines = text_body.split("\n")
        for line in lines:
            if item in [x.lower() for x in line.split()]:
                for word in [x.lower() for x in line.split()]:
                    if "$" in list(word):
                        return "".join(filter(lambda x: x.isdigit(), word))


c = Crawler()
'''
while True:
    df = c.parse_json(c.get_posts(1000))
    c.log(df)
    print(c.get_history("970"))
    time.sleep(60*60)


"""
#Example of data usage:
hist = c.get_history('1080ti')
hist = hist.drop(0)
print(hist.head())
plt.plot(hist['date_created'],hist['price'])
plt.show()
"""
'''


def search_CL(crawler):
    """
        Search craigslist for offers on the items in the itemlist and compare to current prices on /r/hardwareswap
    """
    page = requests.get(
        "https://collegestation.craigslist.org/d/computer-parts/search/syp"
    )

    CLsoup = BeautifulSoup(page.text, "html.parser")

    titles = CLsoup.find_all(class_="result-title")
    prices = CLsoup.find_all(class_="result-price")
    prices = [x for i, x in enumerate(prices) if i % 2]
    titles = [x.contents[0] for x in titles]
    prices = [x.contents[0] for x in prices]

    for t, p in zip(titles, prices):
        for item in crawler.itemlist:
            if item in t.split():
                hist = crawler.get_history(item)
                print(t, p, f"hwswap: {hist['price'][1:3].mean()}")


search_CL(c)
