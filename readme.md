# HardwareFlip

## A tool to track the prices of computer hardware on the secondhand market

Enter the names of the items that you want to track separated by new lines in the 'itemsofinterest.txt' file. The script will update the price database every hour. 